---
slides_folder_name: prototipo-visual
title: Protótipo visual
permalink: prototipo-visual
description: "Protótipo de plataforma online para tutores e médicos veterinários visando auxílio no atendimento (diagnóstico) de papagaios por meio de ferramentas de inteligência artificial. Uma ideia nascente com objetivo de gerar um laudo estatístico do estado de saúde a partir de informações de entrada como sintomas, radiografia, áudio, anamnese ou outros."
reveal_theme: black.css
reveal_transition: default
reveal_options:
  controls: false
  progress: true
  keyboard: false
  touch: false
  overview: false
  slideNumber: true
  hideInactiveCursor: false
  autoSlideStoppable: false
  hash: true
  fragmentInURL: false
extra_css: ../../assets/css/prototipo-ilustrativo.css
---

