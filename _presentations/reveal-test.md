---
slides_folder_name: reveal-test
title: Jekyll and Reveal.js 4.1.0 test
reveal_theme: black.css
reveal_transition: default
reveal_options:
  controls: false
  progress: true
  keyboard: false
  touch: false
reveal_plugins:
  RevealMarkdown
  RevealHighlight
  RevealNotes
  RevealZoom
extra_css: ../../assets/css/prototipo-ilustrativo.css
---
