---
title: "Papaga<span style='color: #7de143;'>iA</span>"
description: "Pesquisa e desenvolvimento de ferramentas online de inteligência artificial para a saúde e preservação de aves psitacídeos, como papagaios e araras."
layout: splash
header:
  og_image: /assets/img/og-banner.jpg
  overlay_image: /assets/img/banner.jpg
  overlay_filter: 0.4
  caption: "Foto por <a href='https://www.flickr.com/photos/arcadius/5695435985/' rel='nofollow noopener' target='_blank'>Arcadiuš</a> (<a href='https://creativecommons.org/licenses/by/2.0/' rel='nofollow noopener' target='_blank'>CC-BY 2.0</a>)"
  actions:
  - label: "Clique aqui e experimente!<i class='fas fa-hand-pointer clicable' style='font-size: 0.75em; padding-left: 0.75em; color: #7de143;'></i>" # faça o teste! Ou Clique aqui para testar o protótipo!
    url: "prototipo-visual"
excerpt: "<span style='color: #7de143;'>i</span>nteligência <span style='color: #7de143;'>A</span>rtificial para saúde e preservação de psitacídeos"
---
 
## Sobre

A PapagaiA tem o objetivo de ser uma plataforma online gratuita de inteligência artificial que auxilie na saúde e preservação de psitacídeos, como papagaios e araras, por exemplo:

<strong style="font-family: 'Roboto'; font-variant: small-caps;">Para tutores</strong> &rarr; Descobrir se a ave é macho ou fêmea a partir de um áudio  
![espectro de áudio](/assets/img/audio.png){: .center-image style="margin-top:0.5em;"}
{: .notice--warning}

<strong style="font-family: 'Roboto'; font-variant: small-caps;">Para veterinários</strong> &rarr; Obter um <a href="assets/files/papagaia-laudo-estatistico.pdf" download>laudo estatístico<i class="fas fa-hand-pointer clicable"></i></a> do estado de saúde da ave a partir de dados não invasivos como fotos, áudio e sintomas.  
<span style="display: block; text-align: center; margin-top: 0.5em;">Conheça o <a href="/prototipo-visual" target="_blank">protótipo ilustrativo<i class="fas fa-hand-pointer clicable"></i></a> e o <a href="assets/files/projeto-pesquisa-diagnostico.pdf" download>projeto de doutorado<i class="fas fa-hand-pointer clicable"></i></a></span>  
&rarr; <a href="https://github.com/rahcor/scc5830-project/blob/main/presentation-notebook.ipynb" target="_blank">Análise da morfologia dos excrementos<i class="fas fa-hand-pointer clicable"></i></a> à partir de uma foto.
{: .notice--warning}

<strong style="font-family: 'Roboto'; font-variant: small-caps;">Para profissionais do meio ambiente</strong> &rarr; Prever regiões de alta probabilidade de captura e tráfico, permitindo adotar medidas antes que os crimes ocorram.
Veja o <a href="assets/files/relatorio-ibama-ml.pdf" download>relatório<i class="fas fa-hand-pointer clicable"></i></a>  
![mapa de locais de apreensão](/assets/img/mapa.png){: .center-image style="width:33%;margin-top:0.5em;"}  
&rarr; Analisar como a paisagem (urbano/rural/vegetação) afeta determinada espécie e propor intervenções, como a <a href="/ecologia-paisagens">utilização de ninhos artificiais<i class="fas fa-hand-pointer clicable"></i></a>.  
&rarr; Modelagens sobre a gripe aviária (veja o <a href="assets/files/avian-influenza-review.pdf" download>relatório de revisão<i class="fas fa-hand-pointer clicable"></i></a>).
{: .notice--warning}

## Por que?

![psitacideos](/assets/img/psitacideos.jpg "Foto por Brian Ralphs@Flickr sob licença CC-BY-2.0"){: .center-image style="width:50%; border-radius: 5%; margin-top: 1.5em;"}

Psitacídeos, como os papagaios e araras, são vistos frequentemente como animais de estimação no Brasil e no mundo, porém essa família de aves possui grande número de espécies ameaçadas de extinção.
Além disso, a contenção e a realização de exames invasivos em aves são procedimentos que envolvem algum risco, principalmente quando a ave está com a saúde debilitada.
Desse modo, é importante buscar soluções para esses e outros problemas.

![smartphone](/assets/img/smartphone.png "Imagem por Katerina Limpitsouni @https://undraw.co/"){: .center-image style="width:25%;"}

O avanço digital das últimas décadas viabiliza uma grande variedade de dados, como fotos, áudios, e sinais de GPS de satélite, que podem vir da internet ou de smartfones nas regiões mais remotas do país.
Ao mesmo tempo, a evolução científica nas áreas de estatística e computação, em particular na ciência de dados, trouxe técnicas que podem ser usadas em conjunto com esses dados para tentar soluções inovadoras e portáteis (aplicativos de smartfones) em favor avifauna silvestre, alcançando desde um exemplar a toda uma população de uma espécie.


## Como?

O PapagaiA ainda é uma ideia nascente e a **linha de pesquisa** desenvolvida é **multidisciplinar**.
Este site foi construído com o objetivo de divulgar o projeto e envolver a comunidade de tutores, médicos veterinários, outros profissionais e institutos de preservação para tomarmos decisões em conjunto no desenvolvimento do projeto e viabilizar a coleta de novos dados.
Portanto, fiquem à vontade para entrar em contato para expor suas expectativas, sugestões e críticas.


### Quem desenvolve o projeto?
Meu nome é Rafael, sou formado em engenharia pela USP com pós-graduação no tema de Bioengenharia (<a href="http://lattes.cnpq.br/6720748394398202" target="_blank">Currículo Lattes<i class="fas fa-hand-pointer clicable"></i></a>).
Decidi iniciar o PapagaiA em homenagem ao papagaio que esteve em nossa companhia por 40 anos.
Espero de alguma maneira retribuir sua companhia através do projeto, contribuindo para a preservação da fauna e fomentando a vida livre dessas espécies.
Além disso, o projeto surge como uma evolução de meu objetivo anterior de atuar em Bioengenharia e pelo gosto em computação e estatística.

## Contato

Um dos princípios do desenvolvimento do PapagaiA é ouvir e aprender, portanto suas expectativas, sugestões e críticas serão sempre bem vindas!  
É possível contato por e-mail (clique para enviar):  
<a href="mailto:papagaia@pm.me" class="contact-link" rel="nofollow noopener noreferrer" target="_blank"><i class="fas fa-envelope-open-text" aria-hidden="true" style="margin-right: 0.4em;"></i>papagaia @ pm . me<i class="fas fa-hand-pointer clicable"></i></a>
por whatsapp (clique no número para acessar):  
<a href="https://wa.me/5516981561953?%5BWebsite%20PapagaiA%5D%20" class="contact-link" rel="nofollow noopener noreferrer" target="_blank"><i class="fab fa-fw fa-whatsapp" aria-hidden="true" style="font-size: 1.3em; vertical-align: bottom; margin-right: 0.05em;"></i>(16) 98156 1953<i class="fas fa-hand-pointer clicable"></i></a>
<!--FORM DESATIVADO TEMPORARIAMENTE 15/06/2022-->
<!--ou pelo formulário abaixo:-->
<!--<form-->
<!--  action="https://formspree.io/f/mrgoyqng"-->
<!--  method="POST" target="_blank"-->
<!--  style="margin-top: -1em; margin-bottom: 2em; padding-top: 0.7em; padding-bottom: 0.75em; overflow: hidden;"-->
<!--<input type="hidden" name="_language" value="pt-BR"/>-->
<!--  <label>Seu e-mail:<input type="text" name="email">-->
<!--  </label>-->
<!--  <label>Sua mensagem:-->
<!--    <textarea name="message"></textarea>-->
<!--  </label>-->
<!--  <div style="line-height: 0;">-->
<!--      <input type="checkbox" id="noticias" name="noticias"><label class="noticias" for="noticias"> &larr; Marque para receber atualizações sobre o PapagaiA por e-mail.</label>-->
<!--  </div>  -->
<!--  <button type="submit" class="btn btn--large btn--link custom" style="margin-left: auto; margin-right: auto; margin-top: 0.5em; display: block;">Enviar<i class="fas fa-hand-pointer clicable" style="font-size: 0.9em; margin-left: 0.5em; vertical-align: baseline;"></i></button>-->
<!--</form>-->

Muito obrigado pela visita! Se você gostou do projeto, por favor divulgue.
<!--Se você gostou do projeto, por favor compartilhe nas redes (clique nos ícones):-->
<!--<ul class="social-icons-share"> -->
<!--<li><a href="https://www.facebook.com/sharer/sharer.php?u=https%3A%2F%2Fwww.papagaia.com.br%2F"-->
<!--  class="btn" title="Compartilhe no Facebook" target="_blank" rel="nofollow noopener">-->
<!--  <i class="fab fa-fw fa-facebook" aria-hidden="true" style="font-size: 4em;"></i>-->
<!--  <i class="sr-only">Facebook</i>-->
<!--</a></li>-->
<!--<li><a href="https://www.linkedin.com/shareArticle?mini=true&url=https%3A%2F%2Fwww.papagaia.com.br%2F"-->
<!--  class="btn" title="Compartilhe no LinkedIn" target="_blank" rel="nofollow noopener">-->
<!--  <i class="fab fa-fw fa-linkedin" aria-hidden="true" style="font-size: 4em;"></i>-->
<!--  <i class="sr-only">LinkedIn</i>-->
<!--</a></li>-->
<!--<li><a href="https://twitter.com/intent/tweet?text=PapagaiA+-+inteligência+Artificial+para+o+atendimento+de+papagaios&url=https%3A%2F%2Fwww.papagaia.com.br%2F"-->
<!--  class="btn" title="Compartilhe no Twitter" target="_blank" rel="nofollow noopener">-->
<!--  <i class="fab fa-fw fa-twitter" aria-hidden="true" style="font-size: 4em;"></i>-->
<!--  <i class="sr-only">Twitter</i>-->
<!--</a></li>-->
<!--</ul>-->
