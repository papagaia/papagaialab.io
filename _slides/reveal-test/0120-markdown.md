---
slides_folder_name: reveal-test
---
## Markdown support
{: .r-fit-text}

<p>Add the <code>r-fit-text</code> class to auto-size text</p>

If filename is `.md`, jekyll will render it in advance of reveal.
{: style="color: red;" :}

Some hack will be allowed then. Just remember to add custom css.
{: .notice--warning}

```[]
<section data-markdown>
  ## Markdown support

  Write content using inline or external Markdown.
  Instructions and more info available in the [docs](https://revealjs.com/markdown/).
</section>
```
