# Sobre o projeto
O PapagaiA tem o objetivo de ser uma plataforma online que auxilie tutores e/ou médicos veterinários no atendimento (diagnóstico) de papagaios.
A intenção é aplicar ferramentas de inteligência artificial e ciência de dados para gerar um laudo estatístico do estado de saúde do papagaio a partir de dados de entrada (sintomas, radiografia, áudio, anamnese ou outros).

Esse é o repositório do código fonte do site.