---
title: "<span style='color: #7de143;'>Ecologia de paisagens</span>"
layout: splash
header:
  og_image: /assets/img/og-banner.jpg
  overlay_image: /assets/img/banner.jpg
  overlay_filter: 0.4
  caption: "Foto por <a href='https://www.flickr.com/photos/arcadius/5695435985/' rel='nofollow noopener' target='_blank'>Arcadiuš</a> (<a href='https://creativecommons.org/licenses/by/2.0/' rel='nofollow noopener' target='_blank'>CC-BY 2.0</a>)"
excerpt: "A ecologia de paisagens permite analisar como determinada espécie interage com áreas urbanas, rurais e de vegetação."
permalink: ecologia-paisagens
---

Pré-projeto de doutorado idealizado para a disciplina Fundamentos em Ecologia de Paisagens ministrada pelo Prof. Dr. Milton Cezar Ribeiro @ UNESP/IB Rio Claro.

## Introdução
Com o avanço das ações antrópicas na ocupação do solo, estão ocorrendo pressões deletérias sobre os habitats e as espécies que neles vivem.
É característico dos psitacídeos a nidificação em ocos de árvores, cuja frequência relativa de ocorrência é baixa, sobretudo para as cavidade maiores que abrigam os psitacídeos de maior porte.
Sob a hipótese da continuidade do avanço desarrazoado do desmatamento decorrente da expansão do agronegócio e do fogo que limitaria esse espaço de nidificação ainda mais, pode a paisagem urbana e rural, com a integração de ninhos artificiais, se tornar um refúgio de nidificação para os psitacídeos de médio e grande porte?

Há duas motivações que permeiam essa possibilidade: a adaptação observada do periquitão/maritaca à nidificação em edificações urbanas/rurais e a cultura brasileira de proximidade com os psitacídeos.

Desse modo, propõe-se utilizar os fundamentos de ecologia de paisagens para analisar e modelar o problema.

## Parte 1 - Pesquisa básica.
Para subsidiar a proposta de integração de ninhos artificiais ao ambiente urbano/rural, é necessário compreender como a paisagem atual interfere na ocupação de ninhos (postura) e na taxa de sucesso da saída dos filhotes do ninho (sucesso populacional) de 3 ssp. de psitacídeos de portes distintos, incluindo o periquitão/maritaca, que já se observa uma integração ao ambiente urbano/rural.

Em uma primeira análise, a nidificação e desenvolvimento dos filhotes pressupõe a existência de abrigo (ninho), temperatura (chocar) e alimentação (aporte nutricional), bem como o efeito negativo de predadores.

Convém rever na bibliografia existente:
* Os fatores a/bióticos e seus efeitos na nidificação, postura e desenvolvimento dos filhotes de psitacídeos.
* A adaptação do periquitão/maritaca ao ambiente urbano/rural.
* A movimentação entre ninho e fonte de alimento e entre área de reprodução e área de nidificação.
* Aplicações de ecologia de paisagem específicas aos psitacídeos

Métodos/Checklist:
* Delimitar ssps.: 3 psitacídeos = {psittacara leucophthalmus (pequeno), amazona aestiva (médio), ara ararauna (grande)} em razão do fator tamanho.
* Delimitar local: Cerrado MS/MT/GO, área em que as 3 ssp ocorrem simultaneamente para bloquear fatores externos. Incluir urbano, rural e vegetação nativa.
* Selecionar idade/período das imagens: início, meio e fim da estação reprodutiva.
* Selecionar espectros das imagens: RGB (classif dos elementos), Uv (se sensível), IR se calor relevante para sel. do local do ninho.
* Selecionar resolução das imagens. Obs: para o periquitão, incluir o tipo de construção como elemento de heterogeneidade da paisagem, portanto não maior que 10m.
* Obtenção das Imagens do local (satélite, mapa, aerofotografia, etc).
* Incluir camadas de informação extras:
  - abióticas (temperatura, vento, química)
  - bióticas (alimentação, predação -- i.e. felinos)
  - de sentidos do animal (ex. sons, cheiro); e
  - veterinárias (saúde, genética, etc)
* Desenho experimental / Localizar pontos amostrais (estimativa geral e refino a posteriori) e tamanho do buffer (escala): modelo estatistico, homogeneização dos fatores, aleatorização, réplicas ou DOE.
  - Seleção da dist. entre pontos amostrais (obs: limitará à 1/2 da max escala de efeito)
  - Classif heterogeneidade da paisagem.
  - Extrair métricas da paisagem (composição, configuração, interações)
* Incluir as camadas de informações das variáveis dependentes (i.e obtidas em campo)
* Realizar as análises estatísticas: outliers, critério de convergência de escala?, verif IID, exploratória PCA, regressão/ANOVA, ML, inferência, limiares

Alguns fatores sugeridos:
* no urbano/rural: tipo de edificação (classificação por imagem), densidade populacional de felinos (pesquisa tipo votação?)
* na vegetação nativa: densidade de ninhos naturais, densidade de alimentos
* tamanho do psitacídeo = {peq, med, grande}
* Imagem UV
* Imagem IR
* Paisagem:
  * Composição:
    - Área de cada classe
  * Configuração:
    - Shannon
  * Borda
  * FFT
Respostas sugeridas:
* Se houve postura no ninho
* Se houve problema no ovo (predação, má formação, etc)
* Qtde filhotes voaram
* Estado de saúde dos genitores e filhotes

[↥ _Retornar ao topo_](#page-title)

## Parte 2 - Fronteira do conhecimento teórico-conceitual-metodológico
Ainda, convém verificar se existem e quais os valores de limiares de parâmetros de paisagem de não retorno populacional, ou seja, a partir do qual a proposta de intervenção com ninhos artificiais se tornaria crucial para manutenção da espécie?
Entretanto convém refinar o conceito de limiar para aplicação em tomada de decisão.
Atualmente o conceito de limiar está estabelecido para inferência (observação da característica da população) (Huggett 2005, Briske et al 2006) entretanto, para a tomada de decisão, pode ser interessante defini-lo em termos probabilísticos.
Por exemplo, considerando um gráfico de riqueza de espécies e fragmentação. Para garantir a biodiversidade pode ser desejável que a cada 10 amostras aleatórias da paisagem, pelo menos 9 tenham riqueza superior ou igual 5.
Qual seria valor mínimo tolerável da fragmentação para atender essa condição mínima?
Ou seja, resolver o problema: P( Riqueza > 5 | Fragmentação < Fragmentação_limite ) > 9/10?


## Parte 3 - Proposta de intervenção
Por fim, propõe-se utilizar os fundamentos da ecologia de paisagens bem como o aprendizado das partes 1 e 2 para elaborar um projeto piloto de implantação de ninhos artificiais urbanos e rurais para psitacídeos de médio e grande porte.

Espera-se com isso:
* verificar o potencial de integração/adaptação ao ambiente urbano e rural de psitacídeos maiores que o periquitão/maritaca
* verificar o potencial de mudança cultural na população pela transposição da ideia de psitacídeo em cativeiro para psitacídeo livre em ninhos e como essa mudança cultural afetaria o tráfico e comércio dessas espécies

Algumas etapas propostas:
* Avaliação do projeto em comitê de ética.
* Identificar/modelar os spots urbanos e rurais mais promissores e os que devem ser evitados para a instalação do ninhos artificiais.
* Ao final do projeto, avaliar como a paisagem interfere na ocupação dos ninhos artificias (postura) e na taxa de sucesso da saída dos filhotes do ninho artificial (sucesso populacional) e comparar com os resultados para parte 1.
* Integração com projetos já existentes (exemplo: arara-azul, papagaio-verdadeiro).
* Promoção de ações educativas estratégicas na cidades visando a transformação da cultura de cativeiro para ninho, mostrando as vantagens do ninho para o cativeiro e os benefícios para a saúde mental de um contato mais próximo à natureza.
* Identificação de desafios como potenciais problemas veterinários e a movimentação para busca de alimento.

Assim, se aproximar da reposta à pergunta feita inicialmente: com a integração de ninhos artificiais, pode a paisagem urbana e rural se tornar um refúgio de nidificação para os psitacídeos de médio e grande porte? Ainda, observar como isso afetaria o problema do tráfico das espécies.

[↥ _Retornar ao topo_](#page-title)
